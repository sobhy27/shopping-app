import { MongoClient } from "mongodb"

const DATABASE = process.env.DATABASE!
const client = new MongoClient(DATABASE)

export async function connect() {
    await client.connect()
    console.log("Connected successfully to server")
}

export const db = client.db("shopping-app")

// collections
export const users = db.collection("users")
