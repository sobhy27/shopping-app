import express from "express"
import { users } from "../db"

interface RequestBody {
    username: string
    password: string
    name: {
        first: string
        last: string
    }
}

const router = express.Router()

router.post("/signin", async (req, res) => {
    const body: RequestBody = req.body

    const user = await users.findOne({
        username: body.username,
    })

    if (user != null) {
        res.json(
            JSON.stringify({
                status: 1,
                msg: "username already exists!",
            }),
        )
        return
    }

    await users.insertOne(body)

    res.statusCode = 200
    res.end()
})

export default router
