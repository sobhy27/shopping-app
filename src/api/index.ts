import express from "express"

import login from "./login"
import signin from "./signin"

const router = express.Router()

router.use(login)
router.use(signin)

export default router
