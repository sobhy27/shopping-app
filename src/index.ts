// load `.env` file
require("dotenv").config()

import express from "express"
import api from "./api"
import * as db from "./db"
import bodyParser from "body-parser"

// connect to database
db.connect().catch(console.error)

const app = express()
const PORT = process.env.PORT!

// parse req body
app.use(bodyParser.json())

// Api
app.use("/api", api)

app.listen(PORT, () => {
    console.log(`App is listenning on port ${PORT}`)
})
